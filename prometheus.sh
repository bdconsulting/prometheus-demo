#!/bin/sh
#Prom build

mkdir /home/centos/Downloads
cd /home/centos/Downloads && curl -LO "https://github.com/prometheus/prometheus/releases/download/0.16.0/prometheus-0.16.0.linux-amd64.tar.gz"
mkdir /home/centos/Prometheus
cd /home/centos/Prometheus
tar -xvzf /home/centos/Downloads/prometheus-0.16.0.linux-amd64.tar.gz
/home/centos/Prometheus/prometheus-0.16.0.linux-amd64/prometheus -version
sudo chown -R centos:centos /home/centos/Prometheus
cd /home/centos/Downloads && curl -LO "https://github.com/prometheus/node_exporter/releases/download/0.11.0/node_exporter-0.11.0.linux-amd64.tar.gz"
mkdir /home/centos/Prometheus/node_exporter
cd /home/centos/Prometheus/node_exporter && tar -xvzf /home/centos/Downloads/node_exporter-0.11.0.linux-amd64.tar.gz
sudo sh -c 'echo "[Unit]
Description=Node Exporter
[Service]
User=centos
ExecStart=/home/centos/Prometheus/node_exporter/node_exporter
[Install]
WantedBy=default.target" >> /etc/systemd/system/node_exporter.service'
sudo systemctl daemon-reload
sudo systemctl enable node_exporter.service
sudo systemctl start node_exporter.service
cd /home/centos/Prometheus/prometheus-0.16.0.linux-amd64
#add any node exporter updates here
sudo sh -c 'echo "scrape_configs:
  - job_name: \"node\"
    scrape_interval: \"15s\"
    target_groups:
    - targets: ['"'"'localhost:9100'"'"','"'"'10.108.3.60:9182'"'"','"'"'10.108.3.44:9182'"'"']" >> /home/centos/Prometheus/prometheus-0.16.0.linux-amd64/prometheus.yml'
nohup ./prometheus > prometheus.log 2>&1 &
cd /home/centos/Prometheus
sudo yum install git ruby ruby-devel sqlite-devel zlib-devel gcc gcc-c++ automake patch -y
git clone https://github.com/prometheus/promdash.git
cd /home/centos/Prometheus/promdash
gem install bundler
bundle install --without mysql postgresql
mkdir /home/centos/Prometheus/databases
echo "export DATABASE_URL=sqlite3:/home/centos/Prometheus/databases/mydb.sqlite3" >> /home/centos/.bashrc
echo "export RAILS_ENV=production" >> /home/centos/.bashrc
. /home/centos/.bashrc
rake db:migrate
rake assets:precompile
bundle exec thin start -d
